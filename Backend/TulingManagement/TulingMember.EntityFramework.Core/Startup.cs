﻿using Furion;
using Furion.DatabaseAccessor;
using Microsoft.Extensions.DependencyInjection;

namespace TulingMember.EntityFramework.Core
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabaseAccessor(options =>
            {
                options.CustomizeMultiTenants();
                options.AddDb<DefaultDbContext>(DbProvider.SqlServer);
                options.AddDb<MultiTenantDbContext, MultiTenantDbContextLocator>(DbProvider.SqlServer);
            }, "TulingMember.Database.Migrations");
        }
    }
}