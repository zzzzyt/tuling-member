﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TulingMember.Core;

namespace TulingMember.Application.Dto
{
    public class PurchaseOrderDto
    {
        public List<cts_PrintTag> printtag { get; set; }
        public string companyname { get; set; }
        public cts_PurchaseOrder order { get; set; }
        public List<cts_PurchaseOrderDetail> detail { get; set; }
    }
    public class PurchaseOrderBackDto
    {
        public List<cts_PrintTag> printtag { get; set; }
        public string companyname { get; set; }
        public cts_PurchaseOrderBack order { get; set; }
        public List<cts_PurchaseOrderDetailBack> detail { get; set; }
    }
}
