﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TulingMember.Core
{
    public class AppStr
    {
        /// <summary>
        /// 默认密码
        /// </summary>
        public const string DeafultPwd = "123456"; 
 
       
        /// <summary>
        /// 销售单
        /// </summary>
        public const string SaleOrderNoKey = "SaleOrderNoKey";
        /// <summary>
        /// 销售退货单
        /// </summary>
        public const string SaleOrderBackNoKey = "SaleOrderBackNoKey";


        /// <summary>
        /// 采购单
        /// </summary>
        public const string PurchaseOrderNoKey = "PurchaseOrderNoKey";
        /// <summary>
        /// 采购退货单
        /// </summary>
        public const string PurchaseOrderBackNoKey = "PurchaseOrderBackNoKey";
        /// <summary>
        /// 付款单-客户
        /// </summary>
        public const string CustomerPayNoKey = "CustomerPayNoKey";
        /// <summary>
        /// 付款单-供应商
        /// </summary>
        public const string SupplierPayNoKey = "SupplierPayNoKey";
        
        /// <summary>
        /// 记账流水号
        /// </summary>
        public const string FinanceKey = "FinanceKey";
         

        /// <summary>
        /// 系统配置
        /// </summary>
        public const string SystemConfig = "SystemConfig";

        /// <summary>
        /// 注册验证码key
        /// </summary>
        public const string RegistCodeKey = "RegistCodeKey";

        /// <summary>
        /// 忘记验证码key
        /// </summary>
        public const string ForgetPwdCodeKey = "ForgetPwdCodeKey";


        /// <summary>
        ///  微信支付证书
        /// </summary>
        public const string CertificateEntryKey = "CertificateEntryKey";
    }
}
